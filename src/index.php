<?php
	require_once("models/database.php");
	require_once("controlers/l10n.php");
	require_once("controlers/notifications.php");
	
	$lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'], 0, 2);

	if( !in_array($lang, Array("fr","en"))) {
		$lang = "en";
	}

	$mh = new MessagesHandler(GetMessages($lang));
	$nh = new Notifications();

	// Main controller

	if (isset($_GET["register"])) {
		require_once("controlers/register.php");
	}
	else if (isset($_GET["CGU"])) {
		require_once("controlers/cgu.php");
	}
	else {
		require_once("controlers/home.php");
	}
?>
