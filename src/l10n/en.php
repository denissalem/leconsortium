<?php
	$messages = Array(

		// Home

		"theConsortiumOfFreeArtists" => "The Consortium of Free Artists",
		"login" => "Login",
		"register" => "Register",
		"searchArtistsOrWorks" => "Search for artists or works...",
		"permanentExhibitions" => "Permanent Exhibitions",
		"comics" => "Comics",
		"photography" => "Photography",
		"sculptAnd3D" => "Sculpture & 3D",
		"phonography" => "Phonography",
		"cinematography" => "Cinematography",
		"othersFreeCommunities" => "Others Free Communities",
		"site" => "Site",
		"about" => "About",
		"FAQ" => "F.A.Q",
		"sourceCode" => "Source Code",
		"contact" => "Contact",

		// Register

		"joinTheConsortium" => "Join the Consortium",
		"loginName" => "Login name",
		"artistName" => "Artist name",
		"password" => "Password",
		"passwordVerification" => "Password verification",
		"emailAddress" => "E-Mail Address",
		"terms" => "Terms",
		"iVeReadAndAcceptTerms" => "I have read and accept the terms and conditions",
		"previousPage" => "Previous page",
		"uMustAcceptTerms" => "You must accept terms.",
		"emptyMandatoryFields" => "Some mandatory fields are empty.",
		"passwordsDoNotMatch" => "Passwords do not match.",
		"passwordTooWeak" => "The password is too weak. It must have at least 8 characters.",
		"aValidationEmailHasBeenSent" => "A validation email has been sent to %mail%!",
		"ConsortiumRegistration" => "Consortium Registration",
		"toCompleteYourRegistration" => "To complete your registration, please visite the following: %validationLink%.",
		"userNameAlreadyTaken" => "This user name is already taken.",
		"userMailAlreadyTaken" => "This e-mail is already taken."
	);
?>
