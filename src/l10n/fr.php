<?php
	$messages = Array(

		// Home

		"theConsortiumOfFreeArtists" => "Le Consortium des Artists Libres",
		"login" => "Connexion",
	    	"register" => "S'inscrire",
	    	"searchArtistsOrWorks" => "Chercher des artistes ou des oeuvres...",
	    	"permanentExhibitions" => "Expositions Permanentes",
	    	"comics" => "Bande-Dessinée",
	    	"photography" =>  "Photographie",
	    	"sculptAnd3D" => "Sculpture & 3D",
	    	"phonography" => "Phonographie",
	    	"cinematography" => "Cinématographie",
	    	"othersFreeCommunities" => "Autres communautés libristes",
	    	"site" => "Site",
	    	"about" => "À propos",
	    	"FAQ" => "F.A.Q",
	    	"sourceCode" => "Code Source",
	    	"contact" => "Contact",
	
		// Register

		"joinTheConsortium" => "Rejoindre le consortium",
		"loginName" => "Identifiant",
		"artistsName" => "Nom d'artiste",
		"password" => "Mot de passe",
		"passwordVerification" => "Vérification du mot de passe",
		"emailAddress" => "Adresse e-mail",
		"terms" => "Conditions d'utilisation",
		"iVeReadAndAcceptTerms" => "J'ai lu et accepté les conditions d'utilisation",
		"previousPage" => "Page précédente",
		"uMustAcceptTerms" => "Vous devez accepter les conditions d'utilisation.",
		"emptyMandatoryFields" => "Certains champs obligatoires sont vides.",
		"passwordsDoNotMatch" => "Les mots de passes ne correspondent pas.",
		"passwordTooWeak" => "Le mot de passe est trop faible, il doit au moins comporter 8 caractères.",
		"aValidationEmailHasBeenSent" => "Un e-mail de validation a été envoyé à l'adresse %mail%!",
		"ConsortiumRegistration" => "Inscription au Consortium",
		"toCompleteYourRegistration" => "Pour compléter votre inscription, veuillez cliquer sur le liens de validation suivat: %validationLink%.",
		"userNameAlreadyTaken" => "Ce nom d'utilisateur déjà utilisé.",
		"userMailAlreadyTaken" => "Cette adresse e-mail est déjà utilisée."
	);
?>
