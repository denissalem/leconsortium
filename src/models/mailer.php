<?php
	function MailTo($to, $subject, $message) {
		$headers = 'From: noreply@consortium.tuxfamily.org' . "\r\n" .
    		'Reply-To: noreply@consortium.tuxfamily.org' . "\r\n" .
    		'X-Mailer: PHP/' . phpversion();
		return mail($to, $subject, $message, $headers);
	}
?>
