<?php
	require_once("models/helpers.php");

	function IsLoginExists(&$db, $value) {
		return IsExists($db, $value, "userName", "users");
	}

	function IsMailExists(&$db, $value) {
		return IsExists($db, $value, "userMail", "users");
	}

	function CreateUser(&$db, $userName, $userPassword, $userMail) {
		$currentDate = date("Y-m-d H:i:s");
		$validationKey = (string) hash("md5", $userName.$userPassword.$userMail.(string)$currentDate);

		$query = $db->prepare(
			"INSERT INTO users (userName,userPassword,userMail,validationkey,confirmed,registrationDate,status,lastlogintry) VALUES(:userName,:userPassword,:userMail,:validationKey,:confirmed, :registrationDate, :status, :lastLoginTry);"
		);

		if(!$query->execute(
			array(
				":userName" => $userName,
				":userPassword" => hash('sha512', $userPassword),
				":userMail" => $userMail,
				":validationKey" => $validationKey,
				":confirmed" => "False",
				":registrationDate" => $currentDate,
				":status" => 1,
				":lastLoginTry" => $currentDate
			)
		)) {
			print_r(
				$query->errorInfo()
			);
		};

		return $validationKey;
	}
?>
