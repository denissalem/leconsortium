<?php
	function IsExists($db, $value, $field, $table) {
		$query = $db->prepare(
			"SELECT * FROM ".$table." WHERE ".$field." = :value"
		);

		$query->execute(
			array(
				":value" => $value
			)
		);

		if ($query->rowCount() != 0) {
			return true;
		}
		return false;
	}
?>
