<?php
	function GetMessages($lang) {
		switch ($lang) {
			case "fr":
				require_once("l10n/fr.php");
				break;
			default:
				require_once("l10n/en.php");
				break;
		}
		return $messages;
	}

	class MessagesHandler {
		private $messages = Array();

		function __Construct($msgs) {
			$this->messages = $msgs;
		}

		function Get($key) {
			return $this->messages[$key];
		}

		function PrintMessage($key) {
			echo $this->messages[$key];
		}
	}
?>
