<?php
	class Input {
	  	private $inputTag = "<input class=\"%class%\" type=\"%type%\" value=\"%value%\" placeholder=\"%placeholder%\" name=\"%name%\"/>";

		function __Construct(&$notifications, $exception, $name, $type, $value = "", $placeholder = "") {
			$this->inputTag = str_replace("%name%", $name, $this->inputTag);
			$this->inputTag = str_replace("%type%", $type, $this->inputTag);
			$this->inputTag = str_replace("%placeholder%", $placeholder, $this->inputTag);

			if (!PostTriggered()) {
				$this->inputTag = str_replace("%class%", "inputNoError", $this->inputTag);
			}
			else {
				if ( isset($_POST[$name]) && !empty($_POST[$name]) ) {
					if ($type == "email") {
						$value = Sanitize($_POST[$name], FILTER_SANITIZE_EMAIL);
					}
					else {
						$value = Sanitize($_POST[$name], FILTER_SANITIZE_STRING);
					}

					$this->inputTag = str_replace("%class%", "inputNoError", $this->inputTag);
		  		}
				else {
					$this->inputTag = str_replace("%class%", "inputError", $this->inputTag);
		  			$notifications->AddNotification(NOTIFICATION_RED_PAPER, $exception); 
				}
			}
			$this->inputTag = str_replace("%value%", $value, $this->inputTag);
			$this->value = $value;
		}

		function GetValue() {
			return $this->value;
		}

		function PrintValue() {
			echo $this->inputTag;
		}
	}
?>
