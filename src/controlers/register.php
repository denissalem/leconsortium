<?php
	require_once("views/notifications.php");
	require_once("controlers/validators.php");
	require_once("controlers/inputs.php");
	require_once("models/mailer.php");
	require_once("models/register.php");

	$required_fields = Array(
		"userName" => "text",
		"password" => "password",
		"passwordVerification" => "password",
		"userMail" => "mail",
		"iVeReadAndAcceptTerms" => "checkbox"
	);

	$inputUserName = new Input($nh, $mh->Get("emptyMandatoryFields"), "userName", "text");
	$inputPassword = new Input($nh, $mh->Get("emptyMandatoryFields"), "password", "password");
	$inputPasswordVerification = new Input($nh, $mh->Get("emptyMandatoryFields"), "passwordVerification", "password");
	$inputUserMail = new Input($nh, $mh->Get("emptyMandatoryFields"), "userMail", "email");
	$inputIVeReadAndAcceptTerms = new Input($nh, $mh->Get("uMustAcceptTerms"), "iVeReadAndAcceptTerms", "checkbox","1");

	if (StepValidation($nh)) {
		PasswordCheck($nh, $mh);
	}

	if (StepValidation($nh)) {
		if(IsLoginExists($dbDriver, $inputUserName->GetValue())) {
			$nh->AddNotification(NOTIFICATION_RED_PAPER, $mh->Get("userNameAlreadyTaken"));
		}
		if(IsMailExists($dbDriver, $inputUserMail->GetValue())) {
			$nh->AddNotification(NOTIFICATION_RED_PAPER, $mh->Get("userMailAlreadyTaken"));
		}
	}

	if (StepValidation($nh)) {
	  	$validationKey = CreateUser(
			$dbDriver,
			$inputUserName->GetValue(),
			$inputPassword->GetValue(),
			$inputUserMail->GetValue()
		);

	  	MailTo(
			$inputUserMail->GetValue(),
			$mh->Get("ConsortiumRegistration"),
			str_replace(
				"%validationLink%",
				"https://consortium.tuxfamily.org/?validation=".$validationKey,
				$mh->Get("toCompleteYourRegistration")
			)
		);

		require_once("views/registered.php");
	}
	else {
		require_once("views/register.php");
	}
?>
