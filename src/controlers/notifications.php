<?php
	class Notifications {
		private $wrappers = Array();
		private $contents = Array();

		function AddNotification($wrapper, $content) {
		  	if(!in_array($content, $this->contents)) {
				array_push($this->wrappers, $wrapper);
				array_push($this->contents, $content);
			}
		}

		function HowMany() {
			return count($this->contents);
		}

		function PrintNotification() {
		  	$index = 0;
			foreach($this->contents as $content) {
				echo str_replace("%notification%", $content, $this->wrappers[$index]);
				$index++;
			}
		}
	}
?>
