<?php

	function PostTriggered() {
		return !empty($_POST);
	}

	function StepValidation(&$nh) {
		if( $nh->HowMany() == 0 && PostTriggered()) {
			return true;
		}
		return false;
	}

	function IsPasswordWeak() {
		if (strlen($_POST["password"]) < 8) {
			return true;
		}
		return false;
	}


	function DoesPasswordsMatch() {
	  	if ($_POST["password"] === $_POST["passwordVerification"]) {
			return true;
		}
		return false;
	}

	function PasswordCheck(&$nh, &$mh) {
		if(!DoesPasswordsMatch()) {
			$nh->AddNotification(NOTIFICATION_RED_PAPER, $mh->Get("passwordsDoNotMatch"));
		}
		if (IsPasswordWeak()) {
			$nh->AddNotification(NOTIFICATION_RED_PAPER, $mh->Get("passwordTooWeak"));
		}
	}

	function Sanitize($input, $flags) {
		$value = trim($input);
		return filter_var($value, $flags);
	}
?>
