<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8"/>
		<title><?php $mh->PrintMessage("theConsortiumOfFreeArtists"); ?></title>
    		<link rel="stylesheet" href="styles/header.css" type="text/css" />
    		<link rel="stylesheet" href="styles/home.css" type="text/css" />
    		<link rel="stylesheet" href="styles/footer.css" type="text/css" />
	</head>
	<body>
		<?php include("header.php"); ?>
		<div id="searchEngine">
			<form>
				<input type="text" placeholder="<?php $mh->PrintMessage("searchArtistsOrWorks"); ?>">
				<input type="submit" value="">
			</form>
		</div>
		<div id="exhibitionType">
			<div class="wrapper">
				<h1><?php $mh->PrintMessage("permanentExhibitions"); ?></h1>
				<a class="exhibitionIconWrapper" id="buttonComic" href="comics">
					<h2><?php $mh->PrintMessage("comics"); ?></h2>
				</a>
				<a class="exhibitionIconWrapper" id="buttonPhotography" href="photograhies">
					<h2><?php $mh->PrintMessage("photography"); ?></h2>
				</a>
				<a class="exhibitionIconWrapper" id="buttonSculptAnd3D" href="sculptsAnd3Ds">
					<h2><?php $mh->PrintMessage("sculptAnd3D"); ?></h2>
				</a>
				<a class="exhibitionIconWrapper" id="buttonPhonography" href="phonography">
					<h2><?php $mh->PrintMessage("phonography"); ?></h2>
				</a>
				<a class="exhibitionIconWrapper" id="buttonCinematography" href="movies">
					<h2><?php $mh->PrintMessage("cinematography"); ?></h2>
				</a>
			</div>
			<?php include("footer.php"); ?>
		</div>
	</body>
</html>
