			<div id="footer">
				<ul>
					<li><?php $mh->PrintMessage("othersFreeCommunities"); ?></li>
					<li><a href="https://zestedesavoir.com/">Zeste de Savoir</a></li>
					<li><a href="https://linuxmao.org/Accueil">Linux MAO</a></li>
					<li><a href="https://blenderclan.tuxfamily.org/html/modules/news/">Blender Clan</a></li>
					<li><a href="https://www.linuxgraphic.org/wp/">Linux Graphic</a></li>
					<li><a href="https://www.tuxfamily.org/">Tuxfamily</a></li>
					<li><a href="https://chatons.org/">Chatons</a></li>
					<li><a href="https://liberapay.com/">Liberapay</a></li>
					<li><a href="http://www.dogmazic.net/">Dogmazik</a></li>
				</ul>
				<ul>
					<li><?php $mh->PrintMessage("site"); ?></li>
					<li><?php $mh->PrintMessage("about"); ?></li>
					<li><?php $mh->PrintMessage("FAQ"); ?></li>
					<li><a href="https://framagit.org/denissalem/leconsortium"><?php $mh->PrintMessage("sourceCode"); ?></a></li>
					<li><?php $mh->PrintMessage("contact"); ?></li>
					<li>-</li>
					<li>-</li>
					<li>-</li>
					<li>-</li>
				</ul>
			</div>

