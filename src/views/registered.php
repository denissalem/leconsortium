<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php $mh->PrintMessage("theConsortiumOfFreeArtists"); ?></title>
		<link rel="stylesheet" href="styles/paperCard.css" type="text/css" />
	</head>
	<body>
		<header>
			<div class="wrapper">
			</div>
		</header>
		<div id="content">
			<div class="wrapper">
				<p><?php echo str_replace("%mail%", $inputUserMail->GetValue(), $mh->Get("aValidationEmailHasBeenSent")); ?></p>
			</div>
		</div>
		<footer>
			<div class="wrapper">
				<a href="?register">Previous page</a>
			</div>
		</footer>
	</body>
</html>

