<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title><?php $mh->PrintMessage("joinTheConsortium"); ?></title>
		<link rel="stylesheet" href="styles/paperCard.css" type="text/css" />
	</head>
	<body>
		<header>
			<div class="wrapper">
			</div>
		</header>
		<div id="content">
			<form class="wrapper" action="?register" method="post">
				<label for="userName"><?php $mh->PrintMessage("loginName"); ?>:</label>
				<p><?php $inputUserName->PrintValue(); ?></p>
				<label for="password"><?php $mh->PrintMessage("password"); ?>:</label>
				<p><?php $inputPassword->PrintValue(); ?></p>
				<label for="passwordVerification"><?php $mh->PrintMessage("passwordVerification"); ?>:</label>
				<p><?php $inputPasswordVerification->PrintValue(); ?></p>
				<label for="userMail"><?php $mh->PrintMessage("emailAddress"); ?>:</label>
				<p><?php $inputUserMail->PrintValue(); ?></p>
				<p><a href="?CGU" target="_blank"><?php $mh->PrintMessage("terms"); ?></a></p>
				<p><?php $mh->PrintMessage("iVeReadAndAcceptTerms"); ?>: <?php $inputIVeReadAndAcceptTerms->PrintValue(); ?></p>
				<input type="submit" value="<?php $mh->PrintMessage("register"); ?>" />
				<?php $nh->PrintNotification(); ?>
			</form>
		</div>
		<footer>
			<div class="wrapper">
				<a href="?home"><?php $mh->PrintMessage("previousPage"); ?></a>
			</div>
		</footer>
	</body>
</html>
