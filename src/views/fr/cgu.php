<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<title>Conditions d'utilisation</title>
		<link rel="stylesheet" href="styles/paperCard.css" type="text/css" />
	</head>
	<body>
		<header>
			<div class="wrapper">
			</div>
		</header>
		<div id="content">
			<div class="wrapper">
				<h1>Conditions d'utilisation</h1>
				<p><s>
				En utilisant ce service vous accéptez de nous céder tous les droits, de façon irrévocable sur votre femme ou
				votre mari, ainsi que sur vos enfants si vous en avez, ou ceux que vous pourriez avoir. L'utilisateur
				accepte également de nous vendre son âme et celles de ses ancêtres. Vous autorisez formellement les informations
				personnels recueilli par Le Consortium des Artistes Libres à faire l'objet d'études marketing afin de vous
				asujétir à de la publicité ciblé, comme par exemple des extanseurs de pénis si vous êtes un homme, ou des produits minceurs
				si vous êtes une femme. Également ces informations pourront être revendu à des tiers ou offert gracieusement au
				gouvernement du pays de rédidence de l'utilisateur.</s></p>
				<p>Toutes les données personnelles de l'utilisateur collecté par le consortium sont privé et ne seront jamais
				divulgué à un tier.</p>
				<p>L'utilisateur peut, s'il le souhaite supprimer son compte et tout le contenu qui lui est associé (sauf pendant la pause déjeuné, entre 12h30 et 14h00).</p>
				<p>Le service est proposé sans aucune garantie. Les personne gérant celui-ci (code source et hébergement)
				ne peuvent être tenu responsable en cas de pépin. On fait tous comme on peut, hein!</p>
				<p>Le service proposé est un agrégateur d'oeuvres ou de ressources artistiques soumises à des licenses libres.
				L'utilisateur s'engage à ne partager que son propre travaille, lui même soumit comme le reste des oeuvres sur le consortium
				à des licenses libres.</p>
				<p>Le service ne propose pas d'hébergement, sauf pour les oeuvres littéraires. L'hébergement des autres médias est à la charge
				de l'utilisateur (voir <a href="">F.A.Q</a>).</p>
				<p>L'utilisateur, s'il a du skillz de 1337 est autorisé à <i>hacker</i> le site (parce que c'est quand même des bonnes barres 
				de rire de pirater des trucs), SANS faire du tord à personne. Il s'engage alors à contribuer à l'ouvrage des développeurs 
				en améliorant ainsi le service compromit.</p>
				<p>Le Consortium comporte un aspect social, il est donc obligatoire de se montrer courtoi et respectueux envers chacun. Sinon ce
				sera le banissement à vie et la réclusion à perpétuité dans le deepweb. Et moi, si j'étais toi, j'éviterais.</p>
				<p>Soyez créatif, soyez généreux et  amusez vous bien ici!</p>
			</div>
		</div>
		<footer>
			<div class="wrapper">
				<a href="?register"><?php $mh->PrintMessage("previousPage"); ?></a>
			</div>
		</footer>
	</body>
</html>
